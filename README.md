# Jessie A. Young's README

Hi, thanks for visiting my README! 

I work at GitLab and I am a Principal Engineer with the [AI-Powered](https://handbook.gitlab.com/handbook/engineering/development/data-science/ai-powered/) and [Foundations](https://handbook.gitlab.com/handbook/engineering/development/dev/foundations/) Stages. 

## About me

I live in Oakland, California, USA. I've been working from home since 2016. My dog Oreo can sometimes be seen walking around in the background of my video calls.

I studied Philosophy and Government and [Colby College](https://www.colby.edu/) in Maine and have also spent significant time as an adult in Portland (Oregon), Santa Fe (New Mexico), Boston, Santiago (Chile), Singapore, and Tokyo. Before I moved to Oakland, I lived in San Francisco for many years. 

I grew up in Palo Alto, California. I think that the origin of my interest in programming was the excellent journalism program in my high school. As a member of the newspaper staff, I learned to write clearly and succinctly and we had intense "production weeks" where we spent long hours laying out out the monthly version of the school paper using InDesign. I loved the combination of individual work (writing) and collaborative work (editing, assembling layout) that putting together a newspaper required, and I continue to love that combination in my career as a software developer.

To say that I am "neat" or "organized" feels like an understatement. I find great joy in organizing, whether the thing I am organizing is a closet, a drawer, a list of issues, or a codebase. 

## My career at GitLab (so far)

I joined GitLab in 2022 as a Staff Engineer on the Authentication and Authorization group. As part of that group, I worked on the MVC for the [Custom Roles](https://docs.gitlab.com/ee/development/permissions/custom_roles.html) and [automatically delete unconfirmed users](https://docs.gitlab.com/ee/administration/moderate_users.html#automatically-delete-unconfirmed-users) features. 

I was promoted to [Principal Engineer](https://handbook.gitlab.com/handbook/engineering/careers/matrix/development/dev/principal/) in 2023 and am currently the Principal for the [Manage](https://handbook.gitlab.com/handbook/engineering/development/dev/manage/) and [AI-Powered](https://handbook.gitlab.com/handbook/engineering/development/data-science/ai-powered/) Stages.

What does a Principal Engineer do, exactly? Well, it depends on the month. Most of the time, my priorities map to OKRs for the AI-Powered and Foundations stages.

## My career before GitLab

Before GitLab, I spent 4.5 years at Salesforce working on [Heroku](https://www.heroku.com/). 

At Heroku, my work included:

- Serving as part of an on-call rotation supporting the public-facing Heroku API as well as a dozen other systems that are central to the Heroku ecosystem.
beta and GA launches of Enterprise Accounts for Heroku. Blog post announcing our work here: https://blog.heroku.com/enterprise-accounts
- Researching, architecting, and implementing (and debugging, of course!) a brand new service for reporting license usage for Enterprise Heroku customers.
- Setting the groundwork for a multi-year effort to streamline logins among different Salesforce products. I used my knowledge of Heroku's existing authentication ecosystem to help craft long-ranging plans for migrating to a centralized auth system.
- Building features to encourage more Heroku users to protect their accounts with Multi-Factor Authentication (MFA).
 
Before Heroku, I spent 3 years in the govtech space working on several projects for [Code for America](https://codeforamerica.org/) and [18F](https://18f.gsa.gov/).

Before govtech, I spent 3 years at [thoughtbot](https://thoughtbot.com/), where I worked with 20+ large tech companies, startups, and founders to build the right product the right way. This required working with clients to identify the problem that needs to be solved, validating the problem statement, and iterating on solutions for that problem through lean methodologies.